# Huginn Agents

A collection of exported Agents and Scenarios (collections of Agents that work together to peform a task) for Huginn.

In many cases, the Agents & Scenarios are sanitized before being posted here, and will require some editing before they can be used.

## Scenario & Agent Notes

### RSS to Mastodon Scenario

This Scenario will check an RSS feed for new items once per interval (default is 2 hours), format each item, and send it to be posted to an account on any Mastodon host (as long as you have access to an account that allows API access).

You will need to add a credential to the Credentials tab, named `mastodon_access_token`, for the PostAgent to use to authenticate itself to the Mastodon server. You can change the credential name but they must match each other - in the Credentials tab and in the PostAgent's `headers` clause. The credential you add to the Credentials tab is the string that appears on the "Your Access Token" line in the Application Info for your app in the Mastodon web interface (see /settings/applications on your Mastodon server).

The APIs of other Fediverse/ActivityPub applications likely differ, and you will likely need to adapt this scenario to make it work for them.

### Temperature Checks Scenario

This scenario performs alerting based on temperature thresholds.

There are two sets of temperature check flows in this scenario: 

1. Has the outdoor temperature crossed the 32°F/0°C threshold in either direction?
2. Is the outdoor temperature between 60°F/18°C and 70°F/22°C, or above/below that range?

The first check flow is a notifier to help me keep on top of things dependent on the freezing/not freezing ambient temperature outdoors - Is it cold enough that the goldfish pond heater needs to be deployed? Is it warm enough to run the pond filter pump? Do I need to refresh outdoor livestock water sources more frequently so they're not frozen?

The second check flow is a notifier to remind me to let fresh air into the house while the outdoor temp is between 60°F/18°C and 70°F/22°C, and to remind me to close the windows once the temperature is outside of that range - so my heating & AC system doesn't expend energy for nothing.

You can use any temperature data source you want, you'll just need to modify the agent, or sub in a different one. It should supply the current temperature in the following format `"temp": [int]`.

The "not null" trigger agent prevents the next agents from evaluating NULL values that are sometimes supplied by the NWS API - some stations can be flaky.

The next layer of trigger agents is binary a decision tree: continue on the Freezing Temps flow if the temp is between -5 and 5 C; continue on the Windows flow if the temp is between 10 and 30 C.

After that, whichever flow is within limits (if any) will be evaluated for above/below or within/out of range depending on the flow. Each of these four triggers will send a different message when triggered, but it'll be the same message each time it is triggered: "it's above freezing", "it's below freezing", "open the windows", or "close the windows"

The Change Detector agents will allow the first freezing/not freezing or open/close the windows message through, and will then 'close the gate' until a different message is presented. This way, a notification is only sent when there's a change in status, and not every half hour when the temperature meets a trigger's conditions.

This scenario uses a Pushover agent to deliver the notification, but you could just as easily point those notifications to a different agent.

You will need two credentials for the weather poll: `nws_station_id` (the 4 letter Station ID for the weather station you want to poll) for the NWS API Call, `email_address` (your email address) for the required NWS API user-agent string.

You will need two credentials if you use the Pushover notification Agent: `pushover_token` and `pushover_user`

### Website Change Monitoring

- No Man's Sky News: monitors the first article on https://nomanssky.com/news (I guess they don't believe in RSS) and passes along the news item URL, Title, and first paragraph when it changes. In my Huginn instance, this gets passed to an email digest, and maybe *this year* I'll know about Expeditions the *first* time they're run.