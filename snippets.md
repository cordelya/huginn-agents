# Reusable Snippets

These snippets are not entire Agents or Scenarios, but are helpful bits I've found along the way

## Templating Snippets

- To insert a line break in a posting payload, use `{% line_break %}`

## Trigger Agent Rules

- To only forward events with field values (aka "NOT NULL")
```json
"rules": [
    {
      "type": "!regex",
      "value": "^$",
      "path": "path"
    }
```
